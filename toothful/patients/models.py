from django.db import models

# Create your models here.
class BaseModel(models.Model):
  created = models.DateTimeField(auto_now_add=True, null=True)
  updated = models.DateTimeField(auto_now=True, null=True)
  deleted = models.BooleanField(default=False, null=True, blank=True)

  class Meta:
    abstract = True

class Patient(BaseModel):
  first_name = models.CharField(max_length=100, null=True)
  last_name = models.CharField(max_length=100, null=True)
  birthdate = models.DateField(null=True)
  gender = models.CharField(max_length=10, null=True)
  phone_number = models.CharField(max_length=15, null=True)
  home_address = models.CharField(max_length=100, null=True)
  email = models.EmailField(max_length=100, null=True)
  birthplace = models.CharField(max_length=100, null=True)
  education = models.CharField(max_length=100, null=True)


class MedicalRecord(BaseModel):
  patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
  appointment_date = models.DateTimeField(null=True)
  description = models.TextField(null=True)
  personal_health = models.JSONField(null=True)
  odontogram = models.JSONField(null=True)
  file = models.FileField(upload_to='medical_records/', null=True)
  appointment_reason = models.TextField(null=True)

class Treatment(BaseModel):
  patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
  first_time = models.BooleanField(default=False, null=True)
  health = models.JSONField(null=True)
  appointment_datetime = models.DateTimeField(null=True)
  procedure_time = models.TimeField(null=True)
  description = models.TextField(null=True)
  odontogram = models.JSONField(null=True)
  file = models.FileField(upload_to='treatments/', null=True)
  appointment_reason = models.TextField(null=True)
  dental_organ = models.CharField(max_length=20, null=True)
  diagnosis = models.TextField(null=True)
  forecast = models.CharField(max_length=50, null=True)
  treatment = models.TextField(null=True)
  instructions = models.TextField(null=True)

